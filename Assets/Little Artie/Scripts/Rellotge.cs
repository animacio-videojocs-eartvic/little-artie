﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Rellotge : MonoBehaviour{

    [Tooltip("Temps inicial en segons")]
    public int tempsInicial;

    [Tooltip("Escala del temps del Rellotge")]
    [Range(-10.0f,10.0f)]
    public float escalaDeTemps = 1;

    private Text myText;
    private float tempsDelFrameAmbTimeScale = 0f;
    private float tempsAMostrarEnSegons = 0f;
    private float escalaDelTempsAlPausar, escalaDeTempsInicial;
    private bool estaPausat = false;

    void Start()
    {

//Establir l'escala del temps original.
    escalaDeTempsInicial = escalaDeTemps;

    //Donar el component.
        myText = GetComponent<Text>();

            //Iniciem la variable que acumula el temps de cada frame
            //amb el valor inicial.
            tempsAMostrarEnSegons = tempsInicial;

        ActualitzarRellotge(tempsInicial);
    }

void Update()

{
    //La primera variable; representa el temps de cada frame considerant l'escala de temps.
    tempsDelFrameAmbTimeScale = Time.deltaTime * escalaDeTemps;

        //La segona variable; acomula el temps que passa per mostrar-lo en el rellotge.
        tempsAMostrarEnSegons += tempsDelFrameAmbTimeScale;

    //La terçera variable; actualitza el rellotge en el temps inicial.
    ActualitzarRellotge(tempsAMostrarEnSegons);

}

public void ActualitzarRellotge(float tempsEnSegons)

{
    int minuts = 0;
    int segons = 0;
    string textDelRellotge;

    //Assegurar que el temps no sigui negatiu.
    if (tempsEnSegons < 0) tempsEnSegons = 0;

        //Calcular minuts i segons.
        minuts = (int)tempsEnSegons / 60;
        segons = (int)tempsEnSegons % 60;

        //Crear la cadena de caràcters amb dos dígits pels minuts i els segons.
        textDelRellotge = minuts.ToString("00") + ";" + segons.ToString("00");

    //Actualitzar l'element de text d'UI amb la cadena de caràcters.
    myText.text = textDelRellotge;
    }
}
