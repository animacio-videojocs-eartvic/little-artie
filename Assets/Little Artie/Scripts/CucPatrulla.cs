﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CucPatrulla : MonoBehaviour
{

  Animator ani;

  public float visionRadius;
 
  public float speed;
  GameObject player;
  Vector3 initialPosition;
  bool parat;

  
  void Start(){

    ani = GetComponent<Animator>();
    player = GameObject.FindGameObjectWithTag("Player");
    initialPosition = transform.position;

    parat = true;

  }
 void Update(){
//Se li dona una posició inicial, la distancia, i transforma la posició perquè pugui girar correctament.
//El valor positiu gira cap a l'esquerra i el valor negatiu cap a la dreta.

  Vector3 target = initialPosition; 
  float dist = Vector3.Distance(player.transform.position,transform.position);
  if(dist<visionRadius)
  {
    ani.SetBool("caminar",true);
    //Segueix al player.
    target=player.transform.position;
    transform.localScale = new Vector3(0.2f, 0.2f, 1);
    parat = false; 
  }

  //Torna a la posició inicial.
  if(dist>visionRadius && !parat)
  {
    target=initialPosition;
    transform.localScale = new Vector3(-0.2f, 0.2f, 1);    
  }

  if(transform.position == initialPosition)
  {
    parat = true;
    ani.SetBool("atacar",false);
  }
  
  //Es fixa la velocitat de l'enemic, 
  //i l'hi ordenem que canvii de posició.
  float fixedSpeed = speed*Time.deltaTime;
  transform.position=Vector3.MoveTowards(transform.position,target,fixedSpeed); 

 }

//Detecta el player quan collisiona amb el trigger.
 void OnTriggerEnter2D(Collider2D col){
   Debug.Log("player detectat");

  }
}
