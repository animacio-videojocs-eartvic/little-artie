﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentPersonatge : MonoBehaviour
{
  Rigidbody2D PlayerRB;
  public float maxVelocitat;
  public float alturasalt;
  Animator PlayerAnim;
  bool GirarPlayer=true;
  SpriteRenderer PlayerRender;
    public float cont;
  void Start()
  {
      PlayerRB=GetComponent<Rigidbody2D>();
      PlayerRender=GetComponent<SpriteRenderer>();
      PlayerAnim=GetComponent<Animator>();

  }
void Update()
{
    if(Input.GetKeyDown(KeyCode.Space))
    {
        GetComponent<Rigidbody2D>().velocity=new Vector2
        (GetComponent<Rigidbody2D>().velocity.x,alturasalt);
    }
    float moure=Input.GetAxis("Horizontal");
    if(moure<0 && !GirarPlayer)
    {
        Girar();
    }
        else if(moure>0 && GirarPlayer)
        {
            Girar();
        }
        PlayerRB.velocity=new Vector2(moure*maxVelocitat,PlayerRB.velocity.y);
        
    }
    void Girar()
        {
            GirarPlayer=!GirarPlayer;
            PlayerRender.flipX=!PlayerRender.flipX;
        }
   


    }




