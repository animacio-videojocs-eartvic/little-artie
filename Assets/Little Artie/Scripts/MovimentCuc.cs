﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimentCuc : MonoBehaviour
{
Animator ani;
    public float visionRadius;
    private float actualPosition;
    private float lastPosition;
    public float vel;
    public float length;
    private float counter;
    private float starPosition;
    GameObject player;
    Vector3 initialPosition;



    // Start is called before the first frame update
    void Start()
    {
        ani = GetComponent<Animator>();
        starPosition = transform.position.x;
        player=GameObject.FindGameObjectWithTag("Player");

    }


    private void Update()
    {
        Vector3 target = initialPosition;
        float dist = Vector3.Distance(player.transform.position, transform.position);
        if (dist > visionRadius)
        {
            ani.SetBool("atacar", false);
            counter += Time.deltaTime * vel;
            transform.position = new Vector2(Mathf.PingPong(counter, length) + starPosition, transform.position.y);
            actualPosition = transform.position.x;
            if (actualPosition < lastPosition) transform.localScale = new Vector3(0.2f, 0.2f, 1);

            if (actualPosition > lastPosition) transform.localScale = new Vector3(-0.2f, 0.2f, 1);

            lastPosition = transform.position.x;
        }
        if (dist < visionRadius)
        {
            ani.SetBool("atacar", true);
            target = player.transform.position;
            transform.localScale = new Vector3(0.2f, 0.2f, 1);
        }
        }
    }

