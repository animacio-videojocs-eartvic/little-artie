﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Animator animator;
    void Awake()
    {
        animator = GetComponent<Animator>();
    }


    void Update()
    {
        bool correr = Input.GetKey(KeyCode.LeftArrow);
        animator.SetBool("Correr",correr);
        bool atac = Input.GetKey(KeyCode.Space);
        animator.SetBool("Atac", atac);

        
        }
    }

