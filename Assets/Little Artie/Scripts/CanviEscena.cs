﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanviEscena : MonoBehaviour
{
    public void CanviarEscena(string nom)
    {
        SceneManager.LoadScene(nom);
    }
    public void Sortir()
    {
        Application.Quit();
    }
}
