﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desplacar_Girar_Minion1 : MonoBehaviour{
  
private float actualPosition;
private float lastPosition;
public float vel;
public float length;
private float counter;
private float starPosition;
    
 


    // Start is called before the first frame update
    void Start(){
      starPosition=transform.position.x;
     }

    // Update is called once per frame
    void OnTriggerEnter(Collider col){

      if(col.tag == "Player"){
        Debug.Log("ha detectat el player");
      }

        counter +=Time.deltaTime*vel;
        transform.position=new Vector2(Mathf.PingPong(counter,length)+starPosition,transform.position.y);
        actualPosition=transform.position.x;
        if (actualPosition < lastPosition) transform.localScale = new Vector3(0.2f, 0.2f, 1) ;
       
        if (actualPosition > lastPosition) transform.localScale = new Vector3(-0.2f, 0.2f, 1);
       
    lastPosition = transform.position.x;

    }
}
