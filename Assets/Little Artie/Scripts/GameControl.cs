﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameControl : MonoBehaviour{

//S'enumeren les vides que han d'entrar en escena.
    public GameObject Vida1,Vida2,Vida3, gameOver;
    public static int health;

    void Start(){

//Posar totes les vides "true", per tenir-les totes a la pantalla de joc
//Enumerar els casos canviant les vides de "true" a "false" perquè desapareixin: break; 
//al collisionar amb l'enemic.

        health = 3;
        Vida1.gameObject.SetActive(true);
        Vida2.gameObject.SetActive(true);
        Vida3.gameObject.SetActive(true);
    }

    void Update(){

//Es dona la condició de les tres vides i s'aciven les tres.
        if(health>3)
        health=3;
        switch(health)
        {
            
        case 3:
        Vida1.gameObject.SetActive(true);
        Vida2.gameObject.SetActive(true);
        Vida3.gameObject.SetActive(true);
        break;

        case 2:
        Vida1.gameObject.SetActive(true);
        Vida2.gameObject.SetActive(true);
        Vida3.gameObject.SetActive(false);
        break;

        case 1:
        Vida1.gameObject.SetActive(true);
        Vida2.gameObject.SetActive(false);
        Vida3.gameObject.SetActive(false);
        break;

        case 0:
        Vida1.gameObject.SetActive(false);
        Vida2.gameObject.SetActive(false);
        Vida3.gameObject.SetActive(false);
        Time.timeScale = 0;
        break;
        
        }
    }
}
